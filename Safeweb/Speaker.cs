﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safeweb
{
    // Classe para informações do palestrante
    class Speaker
    {
        public int time;
        public string name;
        public string title;
        public Speaker(int time, string name, string title)
        {
            this.time = time;
            this.name = name;
            this.title = title;
        }
    }
}
