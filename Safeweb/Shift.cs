﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safeweb
{
    // Classe para manipulação de informações de cada turno do evento
    class Shift
    {
        public int time;
        public int timeLeft;
        public int hour_start;
        public int hour_end;
        public int day;
        public int shift;
        public List<Speaker> listOfSpeakers;

        public Shift(int day, int shift, int hour_start, int hour_end)
        {
            this.day = day;
            this.shift = shift;
            this.hour_start = hour_start;
            this.hour_end = hour_end;
            this.time = (this.hour_end - this.hour_start) * 60;
            this.timeLeft = this.time;
            this.listOfSpeakers = new List<Speaker>();
        }

        public bool checkTimeLeft(int speaker_time){
            return (timeLeft >= speaker_time);
        }

        // adiciona um palestrante ao turno, caso ainda tenha tempo disponível
        public bool addSpeaker(Speaker speaker)
        {
            if (checkTimeLeft(speaker.time))
            {
                this.timeLeft = this.timeLeft - speaker.time;
                this.listOfSpeakers.Add(speaker);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void removeSpeaker(Speaker speaker)
        {
            this.listOfSpeakers.Remove(speaker);
            this.timeLeft -= speaker.time;
        }

        public void clearSpeakers()
        {
            this.listOfSpeakers = new List<Speaker>();
            this.timeLeft = this.time;
        }

        // monta uma string para impressao do turno
        public string PrepareToPrint()
        {
            string content = "";
            int atual_time = this.hour_start * 60;
            foreach (Speaker speaker in this.listOfSpeakers)
            {
                Utils util = new Utils();
                content = content + System.Environment.NewLine + util.ConvertTimeToPrint(atual_time) + " " + speaker.name + ": " + speaker.title + " " + speaker.time + "";
                atual_time += speaker.time;
            }
            return content;
        }

        // retorna o tempo total de palestras alocadas no evento
        public int getTotalTime()
        {
            int totaltime = 0;
            foreach (Speaker speaker in this.listOfSpeakers)
            {
                totaltime += speaker.time;
            }
            return totaltime;
        }
    }
}
