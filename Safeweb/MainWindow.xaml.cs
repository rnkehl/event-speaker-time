﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using Microsoft.Win32;
using System.Collections;

namespace Safeweb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Speaker> listOfSpeakers;
        private List<Shift> listOfShifts;
        int recursiveCall = 0;
        int maxRecursiveCall = 2000;

        public MainWindow()
        {
            InitializeComponent();
            InitializeShifts();
        }

        private void InitializeShifts()
        {
            // Inicialização dos turnos
            this.listOfShifts = new List<Shift>();
            this.listOfShifts.Add(new Shift(1, 1, 9, 12));
            this.listOfShifts.Add(new Shift(1, 2, 13, 17));
            this.listOfShifts.Add(new Shift(2, 1, 9, 12));
            this.listOfShifts.Add(new Shift(2, 2, 13, 17));
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            string inputContent;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                // ao enviar um novo arquivo de entrada
                recursiveCall = 0;
                inputContent = File.ReadAllText(openFileDialog.FileName);
                input.Text = inputContent;
                this.listOfSpeakers = ConvertInputToList(inputContent);
                Schedule();
            }
        }

        // transforma os dados de entrada em objetos para melhor manipulação
        private List<Speaker> ConvertInputToList(String contentInput)
        {
            string[] splitString = contentInput.Split(Environment.NewLine.ToCharArray());
            List<Speaker> listOfSpeakers_ = new List<Speaker>();
            foreach (var speaker_ in splitString)
            {
                if (speaker_ != "")
                {
                    string[] speakerWords = speaker_.Split(' ');
                    string lastItem = speakerWords.Last();
                    if (lastItem.Contains("min"))
                    {
                        speakerWords = speakerWords.Take(speakerWords.Count() - 1).ToArray();
                        string line_without_time = String.Join(" ", speakerWords);
                        string[] name_and_title = line_without_time.Split(':');

                        int speaker_time = Int32.Parse(lastItem.Replace("min", "").Trim());
                        Speaker speaker = new Speaker(speaker_time, name_and_title[0].Trim(), name_and_title[1].Trim());
                        listOfSpeakers_.Add(speaker);
                    }
                }
            }
            return listOfSpeakers_;
        }

        // planejamento do evento
        private void Schedule()
        {
            recursiveCall++;
            // para todos os palestrantes
            foreach (Speaker speaker_ in this.listOfSpeakers)
            {
                var _speaker_added = false;
                foreach (Shift shift_ in this.listOfShifts)
                {
                    // tenta alocar o palestrante em um dos turnos
                    if (shift_.addSpeaker(speaker_))
                    {
                        _speaker_added = true;
                        break;
                    }
                }
                // caso não consiga alocar o palestrante em um dos turnos tenta planejar o evento novamente, em outra ordem
                if(!_speaker_added)
                {
                    reoderList();
                }
            }
            // verifica se os requisitos foram cumpridos
            if (!checkRules())
            {
                if (recursiveCall< maxRecursiveCall)
                {
                    reoderList();
                }
                else
                {
                    // caso não consiga alocar os palestrantes em "maxRecursiveCall" tentativas, exibe uma mensagem de erro
                    PrintErro();
                }
            }
            else
            {
                // estando tudo ok, imprime o calendário do evento
                PrintEvent();
            }

        }

        private bool checkRules()
        {
            foreach (Shift shift_ in this.listOfShifts)
            {
                if (shift_.shift == 1)
                {
                    // verifica se os eventos do turno da manhã duram exatamente 3 horas, ou seja começam as 9h e terminam ao meio-dia
                    if (shift_.getTotalTime() != 180)
                    {
                        return false;
                    }
                }
                else
                {
                    // verifica se os eventos da tarde duram entre 3 e 4 horas, ou seja começam as 13h e terminam entre 16h e 17h.
                    if (shift_.getTotalTime() < 180 || shift_.getTotalTime() > 240)
                    {
                        return false;
                    }
                }

            }
            return true;
        }


        private void PrintEvent()
        {
            string content_to_print = "";
            foreach(Shift shift_ in this.listOfShifts)
            {
                if (shift_.day==1 && shift_.shift==1)
                {
                    content_to_print += "Primeiro Dia:";
                }
                else if(shift_.day == 2 && shift_.shift == 1)
                {
                    content_to_print += System.Environment.NewLine + System.Environment.NewLine + "Segundo Dia:";
                }
                else
                {
                    content_to_print += System.Environment.NewLine + "12:00 Almoço";
                }
                content_to_print = content_to_print + shift_.PrepareToPrint();
                if (shift_.shift == 2)
                {
                    Utils util = new Utils();
                    content_to_print += System.Environment.NewLine + util.ConvertTimeToPrint(780 + shift_.getTotalTime()) + " " + "Dúvidas e Debates";
                }                
            }
            result.Text = content_to_print;
        }

        private void PrintErro()
        {
            result.Text = "Não foi encontrada uma combinação possível em " + maxRecursiveCall.ToString() + " tentativas";
        }

        // reordena randomicamente a lista de palestrantes para tentar gerar o calendário do evento novamente
        private void reoderList()
        {
            List<Speaker> listOfSpeakersNew = new List<Speaker>(); ;
            var rnd = new Random();
            listOfSpeakersNew = this.listOfSpeakers.OrderBy(item => rnd.Next()).ToList();
            this.listOfSpeakers = listOfSpeakersNew;
            foreach (Shift shift_ in this.listOfShifts)
            {
                shift_.clearSpeakers();
            }
            Schedule();
        }
    }
}
