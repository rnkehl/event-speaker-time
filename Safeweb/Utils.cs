﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safeweb
{
    public class Utils
    {
        public string ConvertTimeToPrint(int minutes){
            TimeSpan span = TimeSpan.FromMinutes(minutes);
            return span.ToString(@"hh\:mm");    
        }
    }
}
