﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Safeweb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safeweb.Tests
{
    [TestClass()]
    public class UtilsTests
    {
        [TestMethod()]
        public void ConvertTimeToPrintTest()
        {
            Utils util = new Utils();
            string time_convert = util.ConvertTimeToPrint(780);
            Assert.AreEqual(time_convert, "13:00");
            string time_convert_2 = util.ConvertTimeToPrint(30);
            Assert.AreEqual(time_convert_2, "00:30");
            string time_convert_3 = util.ConvertTimeToPrint(100);
            Assert.AreEqual(time_convert_3, "01:40");
        }
    }
}